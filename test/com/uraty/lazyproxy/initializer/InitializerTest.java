package com.uraty.lazyproxy.initializer;

import org.junit.Test;

import static org.junit.Assert.*;

public class InitializerTest {
    @Test
    public void beforeInitialization() throws Exception {
        Initializer initializer = new Initializer(() -> new Integer(1));
        assertFalse(initializer.isInitialized());
    }

    @Test
    public void manualInit() throws Exception {
        Initializer initializer = new Initializer(() -> new Integer(1));
        assertFalse(initializer.isInitialized());

        initializer.init();
        assertTrue(initializer.isInitialized());
        assertEquals(1, initializer.getSubject());
    }

    @Test
    public void autoInit() throws Exception {
        Initializer initializer = new Initializer(() -> new Integer(1));
        assertFalse(initializer.isInitialized());

        assertEquals(1, initializer.getSubject());
        assertTrue(initializer.isInitialized());

        assertEquals(1, initializer.getSubject());
        assertTrue(initializer.isInitialized());
    }

    private static class BooleanHolder { public boolean value = false; }
    @Test
    public void supplierCallTest() throws Exception {
        BooleanHolder holder = new BooleanHolder();
        Initializer<BooleanHolder> initializer = new Initializer(() -> { holder.value = true; return holder; });

        assertFalse(holder.value);
        assertTrue(initializer.getSubject().value);
        assertTrue(holder.value);
    }

    @Test
    public void nullTest() throws Exception {
        Initializer initializer = new Initializer(() -> null);

        assertFalse(initializer.isInitialized());
        boolean exceptionThrown = false;
        try {
            initializer.getSubject();
        } catch (InitializationFail e) {
            exceptionThrown = true;
        }
        assertTrue(exceptionThrown);
        assertFalse(initializer.isInitialized());
    }
}