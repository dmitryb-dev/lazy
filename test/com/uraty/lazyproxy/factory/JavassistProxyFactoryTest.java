package com.uraty.lazyproxy.factory;

import com.uraty.lazyproxy.assets.TestClass;
import com.uraty.lazyproxy.initializer.LazyInitProxy;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class JavassistProxyFactoryTest {

    @Test
    public void create() throws Exception {
        TestClass testClass = new TestClass();
        Class<TestClass> proxyClass = new JavassistProxyFactory().create(TestClass.class);
        TestClass proxy = proxyClass.newInstance();
        ((LazyInitProxy) proxy).lazy$setSupplier( () -> {
            testClass.modifyA();
            return testClass;
        });

        assertFalse(testClass.a);
        assertFalse(testClass.getA());
        assertFalse(proxy.a);

        proxy.modifyA();
        assertTrue(testClass.a);
        assertTrue(testClass.getA());
        assertFalse(proxy.a);
        assertTrue(proxy.getA());
    }

    @Test
    public void multipleClasses() throws Exception {
        Class<TestClass> proxyClassA = new JavassistProxyFactory().create(TestClass.class);
        Class<TestClass> proxyClassB = new JavassistProxyFactory().create(TestClass.class);

        assertNotEquals(proxyClassA, proxyClassB);
    }

}

