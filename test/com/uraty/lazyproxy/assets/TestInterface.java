package com.uraty.lazyproxy.assets;

public interface TestInterface {
    void modifyA();
    boolean getA();
}
