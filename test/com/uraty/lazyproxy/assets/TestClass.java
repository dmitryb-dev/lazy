package com.uraty.lazyproxy.assets;

public class TestClass implements TestInterface {
    public boolean a;
    public void modifyA() {
        a = true;
    }
    public boolean getA() { return a; };
}