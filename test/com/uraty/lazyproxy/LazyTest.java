package com.uraty.lazyproxy;

import com.uraty.lazyproxy.assets.TestClass;
import com.uraty.lazyproxy.assets.TestInterface;
import org.junit.Test;

import static org.junit.Assert.*;

public class LazyTest {
    @Test
    public void proxyFor() throws Exception {
        TestClass testClass = Lazy.proxyFor(TestClass.class, () -> new TestClass());

        assertFalse(testClass.a);
        assertFalse(testClass.getA());

        testClass.modifyA();

        assertFalse(testClass.a);
        assertTrue(testClass.getA());
    }

    @Test
    public void interfaceTest() throws Exception {
        TestInterface testInterface = Lazy.proxyFor(TestInterface.class, () -> new TestClass());

        testInterface.modifyA();
        assertTrue(testInterface.getA());
    }

    @Test
    public void cacheTest() throws Exception {
        TestClass testClassA = Lazy.proxyFor(TestClass.class, () -> new TestClass());
        TestClass testClassB = Lazy.proxyFor(TestClass.class, () -> new TestClass());

        assertNotEquals(testClassA, testClassB);
        assertEquals(testClassA.getClass(), testClassB.getClass());
    }

}