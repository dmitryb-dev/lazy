package com.uraty.lazyproxy.scala

import com.uraty.lazyproxy.assets.{TestClass, TestInterface}
import com.uraty.lazyproxy.initializer.LazyInitProxy
import org.junit.{Assert, Test}

class LazyTest {
  @Test
  def interfaceTest() {
    val testInterface: TestInterface = Lazy[TestInterface](() => new TestClass)

    Assert assertFalse(testInterface.getA)
    testInterface.modifyA
    Assert assertTrue(testInterface.getA)
  }

  @Test
  def classTest() {
    val testClass: TestClass = Lazy(() => new TestClass)

    testClass.modifyA
    Assert assertTrue(testClass.getA)
  }

  @Test
  def implicitCreateTest(): Unit = {
    val testInterface: TestInterface = Lazy[TestClass]

    testInterface.modifyA()
    Assert.assertTrue(testInterface.getA)
  }
}
