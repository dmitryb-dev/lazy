package com.uraty.lazyproxy.scala

import scala.reflect.ClassTag

object Lazy {
  def apply[T](supplier: () => T)(implicit tag: ClassTag[T]): T =
    com.uraty.lazyproxy.Lazy.proxyFor(
      tag.runtimeClass.asInstanceOf[Class[T]],
      () => supplier())

  def apply[T](implicit tag: ClassTag[T]): T =
    com.uraty.lazyproxy.Lazy.proxyFor(
      tag.runtimeClass.asInstanceOf[Class[T]],
      () => tag.runtimeClass.newInstance().asInstanceOf[T])
}