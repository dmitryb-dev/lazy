package com.uraty.lazyproxy;

import com.uraty.lazyproxy.factory.JavassistProxyFactory;
import com.uraty.lazyproxy.factory.LazyInitProxyFactory;
import com.uraty.lazyproxy.initializer.LazyInitProxy;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class Lazy {
    public static <T> T proxyFor(Class<T> subject, Supplier<? extends T> supplier) {
        try {
            Class proxyClass = getProxy(subject);
            Object instance = proxyClass.newInstance();
            ((LazyInitProxy) instance).lazy$setSupplier(supplier);
            return (T) instance;
        } catch (Exception e) {
            throw new IllegalStateException("Cannot instantiate proxy", e);
        }
    }

    private static <T> Class getProxy(Class<T> subject) {
        Class proxy = cache.get(subject);
        if (proxy == null) {
            proxy = factory.create(subject);
            cache.put(subject, proxy);
        }
        return proxy;
    }

    private final static LazyInitProxyFactory factory = new JavassistProxyFactory();
    private final static Map<Class, Class> cache = new HashMap<>();
}
