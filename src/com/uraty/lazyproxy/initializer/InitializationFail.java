package com.uraty.lazyproxy.initializer;

public class InitializationFail extends RuntimeException {
    public InitializationFail() {
    }

    public InitializationFail(String message) {
        super(message);
    }

    public InitializationFail(String message, Throwable cause) {
        super(message, cause);
    }

    public InitializationFail(Throwable cause) {
        super(cause);
    }
}
