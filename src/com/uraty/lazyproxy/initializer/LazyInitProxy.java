package com.uraty.lazyproxy.initializer;

import com.sun.istack.internal.NotNull;

import java.util.function.Supplier;

public interface LazyInitProxy<T> {
    void lazy$setSupplier(@NotNull Supplier<T> supplier);
}
