package com.uraty.lazyproxy.initializer;

import com.sun.istack.internal.NotNull;
import java.util.function.Supplier;

public class Initializer<T> {

    public Initializer(@NotNull Supplier<T> supplier) {
        this.supplier = supplier;
    }

    public void init() {
        if (isInitialized) return;
        synchronized (this) {
            if (isInitialized) return;
            if ((subject = supplier.get()) == null) throw new InitializationFail("Supplier returned null");
            isInitialized = true;
        }
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    public T getSubject() {
        init();
        return subject;
    }

    private T subject;
    private boolean isInitialized = false;
    private Supplier<T> supplier;
}
