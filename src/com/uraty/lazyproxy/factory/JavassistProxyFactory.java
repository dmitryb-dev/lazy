package com.uraty.lazyproxy.factory;

import com.uraty.lazyproxy.initializer.Initializer;
import com.uraty.lazyproxy.initializer.LazyInitProxy;
import javassist.*;

import java.util.function.Supplier;

public class JavassistProxyFactory implements LazyInitProxyFactory {

    @Override
    public <T> Class<T> create(Class<T> subject) {
        try {
            final CtClass proxy = this.subclassFor(subject);
            declareInitializer(proxy);
            overrideMethods(proxy, subject);
            implementInterface(proxy);
            resetAbstract(proxy);
            return proxy.toClass();
        } catch (NotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (CannotCompileException e) {
            throw new IllegalStateException(e);
        }
    }

    private CtClass subclassFor(Class subject) throws NotFoundException, CannotCompileException{
        final CtClass parent = pool.get(subject.getName());
        final CtClass proxy = pool.makeClass("lazy.proxy.generated." + parent.getSimpleName() + "$LazyInitProxy$" + id++);
        if (subject.isInterface())
            proxy.addInterface(parent);
        else
            proxy.setSuperclass(parent);

        for (CtMethod method : parent.getMethods()) {
            if (method.getName().equals("finalize")) continue;
            if  (((Modifier.PRIVATE | Modifier.STATIC | Modifier.FINAL | Modifier.NATIVE)
                    & method.getModifiers()) > 0)
                continue;
            proxy.addMethod(CtNewMethod.copy(method, proxy, null));
        }
        return proxy;
    }

    private void declareInitializer(CtClass proxy) throws CannotCompileException {
        proxy.addField(CtField.make(
                String.format("private %s %s;", Initializer.class.getName(), INITIALIZER_FIELD_NAME),
                proxy));
    }

    private void implementInterface(CtClass proxy) throws NotFoundException, CannotCompileException {
        proxy.addInterface(pool.get(LazyInitProxy.class.getName()));
        proxy.addMethod(CtMethod.make(String.format(
                "public void lazy$setSupplier(%s supplier) {" +
                        "this.%s = new %s(supplier);" +
                "}", Supplier.class.getName(), INITIALIZER_FIELD_NAME, Initializer.class.getName()),
                proxy));
    }

    private void overrideMethods(CtClass proxy, Class type) throws CannotCompileException, NotFoundException {
        for (CtMethod method : proxy.getDeclaredMethods()) {
            String body = method.getReturnType().equals(CtClass.voidType)? "" : "return ";
            body += String.format("((%s)%s.getSubject()).%s($$);",
                    type.getName(),
                    INITIALIZER_FIELD_NAME,
                    method.getName());
            method.setBody('{' + body + '}');
        }
    }

    private void resetAbstract(CtClass proxy) {
        proxy.setModifiers(proxy.getModifiers() & ~Modifier.ABSTRACT);
    }

    private static final ClassPool pool = ClassPool.getDefault();
    private static final String INITIALIZER_FIELD_NAME = "$initializer";
    private static int id = 0;
}
