package com.uraty.lazyproxy.factory;

public interface LazyInitProxyFactory {
    <T> Class<T> create(Class<T> subject);
}
