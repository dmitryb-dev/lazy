## Lazy init proxy provider ##

This library can be used for tho purposes:

* Lazy initialization

* Asynchronous load

It uses *javassist* library for making proxy. And of course first making of proxy is slowly due compilation. Facade class *Lazy* uses cache and next proxy getting for same class will be very fast.

### How to use ###
**Java**

```
#!java

YourClass yourClass = Lazy.proxyFor(YourClass.class, () -> new YourClass());
```

The fist param *YourClass.class* is a class for proxy. How you can see, you just obtain a simple object of *YourClass*. Actually *Lazy* returns a proxy object, that implements interface *LazyInitProxy* and extends YourClass.

Next you can simply use your class as you usually do it. But when you first time call a method of *yourClass* will be invoked *init function*, that you pass as second parameter:
```
#!java

() -> new YourClass()
```

This function must return object that extends or is a instance of class given as first parameter.
And you can give like a first parameter an interface (it's even more preferred):

```
#!java

YourInterface yourInterface = Lazy.proxyFor(YourInterface.class, () -> new YourClass());
```

Of course *YourClass* must implement *YourInterface*.

**Scala**

It's very similar for java, so there is a more handy way to use it.
Making proxy of *YourClass*:

```
#!scala

val yourClass: YourClass = Lazy(() => new YourClass)
```

If you want to get an interface:

```
#!scala

val yourInterface: YourInterface = Lazy[YourInterface](() => new YourClass)
```

The first example (when you just create a new instance of *YourClass* with a default constructor) can be simplified:

```
#!scala

val testInterface: TestInterface = Lazy[TestClass]
```

### Asynchronous load example ###

Suppose you have a asynchronous operation like this (for example "long operation" can be a DB request):

```
#!java

FutureTask<YourClass> asyncTask = new FutureTask<>(() -> { /* A long operation here. And as result we obtain an YourClass object */ });
new Thread(asyncTask).start; // start long operation.
YourClass yourClass = Lazy.proxyFor(YourClass.class, () -> asynTask.get() ); 
// Getting a proxy, that waiting for result if you want to make something with object.
/*
   Do something there, when long operation is going on.
*/
yourClass.someMethod(); // And only there thread will be stopped until result not be get.
```

You can ask "why I need it? I can simply call *asynTask.get()* instead". Yes. But when you have a method, that returns object:

```
#!java

public YourClass load() { ... }
```

You can't return *FutureTask* object, can you? User can use your method and even doesn't know that it asynchronous.

In other way you can combine Lazy objects and load it particulary. Like there:

```
#!java

FutureTask<YourClass> asyncTask = new FutureTask<>(() -> {
/* A long operation here.
   And like result we create an YourClass object: */
   return new YourClass(1, "second", Lazy.proxyFor(Third.class, () -> /* loading a Third object */)); 
   // And there we give Lazy proxy like parameter!
});
```

And if you have a method *loadThird()* that returns lazy proxy for *Third* object last line will looks like:

```
#!java

return new YourClass(1, "second", Lazy.proxyFor(Third.class, () -> loadThird()));
```

So... This code gives you proxy for proxy... First taken from *loadThird()* and describes a DB request. You won't obtain object until it hasn't loaded. And second taken from return statement. It means that you won't make DB request until you try to apply some method of *Third*. Yes in this case it's redundantly. You should be attentive to avoid a big chains of proxy.